dialer-app hacking guide
========================

Building the app
----------------

The list of build requirements can be obtained from the debian/control file.

This app is built using cmake. Here's an example on how to build it:

    mkdir build
    cd build
    cmake ..
    make -j $(nproc)


Running the built app
---------------------

After building the app, it can be run with:

    ./src/lomiri-dialer-app


Test coverage
-------------

**NOTE**: autopilot is currently not available in UBports and the instructions
below are no longer accurate.

dialer-app has autopilot tests which cover basic use cases like making an
outgoing call and receiving an incoming call. They are integration tests which
cover dialer-app itself, all the user session APIs below it (libofono-qt,
telephony-service, telepathy-ofono), and the ofono service on the system D-BUS.
They assume that ofono has been set up with the "phonesim" driver to avoid
depending on any particular hardware; if phonesim is not set up, the tests will
be skipped.

The tests should run on both a desktop and a phone environment.


Running the autopilot tests with the installed app
--------------------------------------------------

The easiest and standard way across Ubuntu phablet packages is to install the
dialer-app-autopilot package and run the tests from the system Python path:

    $ sudo apt-get install lomiri-dialer-app-autopilot
    $ autopilot3 run lomiri_dialer_app

dialer-app-autopilot depends on ofono-phonesim-autostart to set up the
simulated Modem automatically, and all the other additional test dependencies
for autopilot. You can also run the tests right out of the source tree for
development:

    $ cd tests/autopilot
    $ autopilot3 run lomiri_dialer_app

(but you still need all the lomiri-dialer-app-autopilot dependencies installed).


Running the autopilot tests with the built app
----------------------------------------------

After building the app, the autopilot tests can be run with:

    $ make test-autopilot
