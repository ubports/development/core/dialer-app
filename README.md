Lomiri Dialer App
=================
Dialer Application for Ubuntu Touch.

Internals
=========

Dialer app relies on :
  - [telephony-service](https://gitlab.com/ubports/development/core/lomiri-telephony-service) for call relay.
  - [history-service](https://gitlab.com/ubports/development/core/lomiri-history-service) for call history
  - [address-book-app](https://gitlab.com/ubports/development/core/lomiri-address-book-app) for contact search.



Building with clickable
=======================
Install [clickable](http://clickable.bhdouglass.com/en/latest/), then run:

```
clickable
```

For faster build speeds, building app tests is disabled in ```clickable.yaml```


Tests
=====

for QML tests, run `clickable test`
for integration tests, see the HACKING file

i18n: Translating Lomiri's Dialer App into your Language
========================================================

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-dialer-app

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
